const gulp = require('gulp');
const sass = require("gulp-sass");
const browser = require("browser-sync");
const sourcemaps = require("gulp-sourcemaps");
const uglify = require("gulp-uglify");

const init = function (cb) {
  browser.init({
    server: './',
    open: false,
    port: 3000
  });

  cb();
}

const style = function (cb) {
  return gulp.src('asset/src/style/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(sourcemaps.write('./', { includeContent: false }))
    .pipe(gulp.dest('asset/dist/style'))
    .pipe(browser.stream({ match: '**/*.css' }));
}

const reload = function (cb) {
  browser.reload();

  cb();
};

const watch = gulp.series(init, function (cb) {
  gulp.watch(['asset/src/style/**/*.scss'], style);
  gulp.watch(['asset/src/script/*.js'], reload);
  gulp.watch(['*.html'], reload);
  cb();
});

const script = function (cb) {
  return gulp.src('asset/src/script/script.js')
    .pipe(uglify())
    .pipe(gulp.dest('asset/dist/script/'));
}

const build = gulp.series(style, script);

exports.default = gulp.series(style, script, watch);
exports.style = style;
exports.build = build;
